/**

  @description
    create empty [object] or [array] depend of [target]

  @param {object/array} target
  @return {object}

  @example "as array"
    let result = empty([1, 5, 12])
    result == []

  @example "as object"
    let result = empty({ first: 1, second: 5, other: 12 })
    result == {}

*/
module.exports = function(target){
  return Array.isArray(target) ? [] : {}
}
