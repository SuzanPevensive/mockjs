/**

  @description
    create [dummy object] filled specific [fields] with [def] as [values]

  @param {array}    fields - array of [dummy] fields
  @param {any}      def - default value for [dummy] fields
  @return {object}

  @example
    let result = dummy([`name`, `surname`, `address`], `...`)
    result == { name: `...`, surname: `...`, address: `...` }

*/
module.exports = (fields, def = null) => {
  let dummy = {}
  fields.forEach((field) => {
    dummy[field] = def
  });
  return dummy
}
