/**

  @description
    create [mock object] from [object] source
      fields will be have default values depends of type
      method will return the owner

  @param {object}   object - object as source
  @return {object}

  @example

    class Person{

      constructor(){
        this.name = "Suzan"
        this.__age = 23
      }

      age(){
        return this.__age
      }

   }

    let result = mock(new Person())
    result.name == ""
    result.age() == result

*/
//@browserify:getter
const { when } = require(`@suzan_pevensive/when-js`)
module.exports = (object, values = {})=>{
    if(object == null) return null

    function mockValue(value){
      //@browserify:get(when)
      return when(typeof value, {
        boolean: ()=>false,
        number: ()=>0,
        string: ()=>'',
        function: ()=>(()=>proxy),
        undefined: ()=>undefined,
        else: ()=>mock(value)
      })
    }

    let handler = {
        get: function(target, name) {
            return values[name] || mockValue(target[name])
        }
    };

    let proxy = new Proxy(object, handler)
    return proxy;

}
