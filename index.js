const path = require(`path`)

const { browserify } = require(`@suzan_pevensive/browserify-js`)

module.exports = {

  empty: require(`./src/empty`),
  dummy: require(`./src/dummy`),
  mock: require(`./src/mock`),

  browserify: (app) => browserify.publishModule(app, `MockJs`, path.join(__dirname, `src`), `js`)

}
